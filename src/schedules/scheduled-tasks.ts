import * as schedule from 'node-schedule';

// FORMAT 
// *    *    *    *    *    *
// ┬    ┬    ┬    ┬    ┬    ┬
// │    │    │    │    │    │
// │    │    │    │    │    └ day of week (0 - 7) (0 or 7 is Sun)
// │    │    │    │    └───── month (1 - 12)
// │    │    │    └────────── day of month (1 - 31)
// │    │    └─────────────── hour (0 - 23)
// │    └──────────────────── minute (0 - 59)
// └───────────────────────── second (0 - 59, OPTIONAL)
// 
// EXAMPLES
// '42 * * * *' -> when the minute is 42 (e.g. 19:42, 20:42, etc.)
// '0 17 ? * 0,4-6 -> every 5 Minutes = */5 * * * *

// fixed time
var rule = new schedule.RecurrenceRule();
rule.second = 15;

/** Run every second 1 */
schedule.scheduleJob('1 * * * * *', () => {
    console.log('Task Runned!');
});