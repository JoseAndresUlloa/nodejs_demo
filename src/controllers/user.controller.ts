import {  UserService } from "../services/user.service";
import { JsonController, Param, Body, Get, Post, Put, Delete } from "routing-controllers";
import { User } from "../entities/user";
 
@JsonController()
export class UserController {
    private service: UserService = new UserService();

    @Get('/users')
    list(){
        return this.service.getAll()
    }

    @Get('/users/:id')
    get(@Param('id') id: number){
        return this.service.getOne(id)
    }

    @Post('/users/sign-up')
    create(@Body() user: User){
        return this.service.create(user)
    }

    @Put('/users/:id')
    update(@Param('id') id: number, @Body() user: User){
        return this.service.update(id, user)
    } 

    @Delete('/users/:id')
    delete(@Param('id') id: number){
        return this.service.delete(id)
    }
}