import { BusinessPartnerService } from "../services/business-partner.service";
import {JsonController, Param, Body, Get, Post, Put, Delete} from "routing-controllers";
import { BusinessPartner } from "../entities/business-partner";
 
@JsonController()
export class BusinessPartnerController {
    private service: BusinessPartnerService = new BusinessPartnerService();

    @Get('/businesspartners')
    list(){
        return this.service.getAll()
    }

    @Get('/businesspartners/:id')
    get(@Param('id') id: number){
        return this.service.getOne(id)
    }

    @Get('/businesspartners/cardcode/:cardCode')
    getByCardCode(@Param('cardCode') cardCode: string){
        return this.service.getByCardCode(cardCode)
    }

    @Post('/businesspartners/sign-up')
    create(@Body() bp: BusinessPartner){
        return this.service.create(bp)
    }

    @Put('/businesspartners/:id')
    update(@Param('id') id: number, @Body() bp: BusinessPartner){
        return this.service.update(id, bp)
    } 

    @Delete('/businesspartners/:id')
    delete(@Param('id') id: number){
        return this.service.delete(id)
    }
}