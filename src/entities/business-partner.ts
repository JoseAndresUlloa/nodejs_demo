import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";
import { SimpleColumnType } from "typeorm/driver/types/ColumnTypes";

@Entity({name: "BusinessPartners"})
export class BusinessPartner {
    
    @PrimaryGeneratedColumn({name: "BPId"})
    id: number

    @Column({ name: "MigrationId", nullable: true })
    migrationId: number = null

    @Column({ name: "ReferenceId1", nullable: true })
    referenceId1: string = null

    @Column({ name: "ReferenceId2", nullable: true })
    referenceId2: string = null

    @Column({ name: "BPName" })
    bpName: string

    @Column({ name: "IsOwner" })
    isOwner: boolean

    @Column({ name: "IsOwner" })
    isActive: boolean
    
    @Column({ name: "IsOwner", nullable: true })
    logo: string = null

    @Column({ name: "CountryId", nullable: true })
    countryId: number = null

    @Column({ name: "StateId", nullable: true })
    stateId: number = null

    @Column({ name: "CountyId", nullable: true })
    countyId: number = null

    @Column({ name: "CityId", nullable: true })
    cityId: number = null

    @Column({ name: "AddressLine1", nullable: true })
    addressLine1: string = null

    @Column({ name: "AddressLine2", nullable: true })
    addressLine2: string = null

    @Column({ name: "PostalCode", nullable: true })
    postalCode: string = null

    @Column({ name: "LocationUrl", nullable: true })
    locationUrl: string = null

    @Column({ name: "LocationMap", nullable: true })
    locationMap: string = null

    @Column({ name: "PhoneNumber1" })
    phoneNumber1: string

    @Column({ name: "PhoneNumber2" })
    phoneNumber2: string

    @Column({ name: "MobilePhone" })
    mobilePhone: string

    @Column({ name: "Email1" })
    email1: string

    @Column({ name: "Email2" })
    email2: string

    @Column({ name: "BPCurrency" })
    bpCurrency: string

    @Column({ name: "WebsiteUrl" })
    websiteUrl: string

    @Column({ name: "Facebook" })
    facebook: string

    @Column({ name: "Twitter" })
    twitter: string

    @Column({ name: "Twitter" })
    instagram: string

    @Column({ name: "SkypeContact" })
    skypeContact: string

    @Column({ name: "Coordinates", nullable: true })
    coordinates: SimpleColumnType = null

    @Column({ name: "createdBy" })
    createdBy:	number

    @Column({ name: "CreationDate" })
    creationDate: Date;

    @Column({ name: "Description" })
    description: string

    @Column({ name: "Annotations", nullable: true })
    annotations: string
}