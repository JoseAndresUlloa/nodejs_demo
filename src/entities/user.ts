import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity({name: "Users"})
export class User {

    @PrimaryGeneratedColumn({name: "UserId"})
    id: number;

    @Column({ name: "ReferenceId1", nullable: true })
    referenceId1: string = null;

    @Column({ name: "ReferenceId2" })
    referenceId2: string = null;

    @Column({ name: "Username" })
    username: string = null;

    @Column({ name: "Name" })
    name: string = null;

    @Column({ name: "Lastname1" })
    lastname1: string = null;

    @Column({ name: "Lastname2" })
    lastname2: string = null;

    @Column({ name: "Password" })
    password: string = null;

    @Column({ name: "Birthday", nullable: true })
    birthday: Date = null;

    @Column({ name: "Gender", nullable: true  })
    gender: string = null;

    @Column({ name: "LogInId" })
    logInId: string = null;

    @Column({ name: "UserType" })
    userType: string = null;

    @Column({ name: "IsActive" })
    active: boolean = false;

    @Column({ name: "BPId" })
    bpId: number = null;
    
    @Column({ name: "CreatedBy", nullable: true })
    createdBy: number = null;

    @Column({ name: "CreationDate", nullable: true })
    creationDate: Date = null;

    @Column({ name: "RoleId" })
    roleId: number = null;
}
