import { verifyJWTToken, createJWToken } from './jwt'
import { User } from '../entities/user';
import * as bcrypt from 'bcryptjs';
import { UserService } from '../services/user.service';

/**
 * Validate the token in the header of the request
 * @param req 
 * @param res 
 * @param next 
 */
export function authenticate(req, res, next) {
  	let token =  req.header('Authorization');

	verifyJWTToken(token).then(decodedToken => {
		req.user = decodedToken;
		next();
	})
    .catch((err) => {
      	res.status(401).json({message: "Invalid auth token provided."})
    });
}

/**
 * Check user using password and username, create a token if credentials are valid
 * @param req 
 * @param res 
 * @param next 
 */
export function login(req, res, next){
	let userService: UserService = new UserService();

	let user: User = req.body;
	return new Promise(async (resolve, reject) =>{ 
		try {
			let userFound: User = await userService.getByUsername(user.username);

			if(!userFound)
				throw new Error("User doesn't exist");

			let result = await bcrypt.compare(user.password, userFound.password);
			
			if(result){
				// create token and return
				res.status(200).json({
					token: createJWToken({
						sessionData: userFound,
						maxAge: 3600
					})
				}
				);
			} else {
				throw new Error("Validation failed. Given email and password aren't matching.");
			}   
		} catch(err){
			next(err);
		}
	});  
}
