import * as jwt from 'jsonwebtoken';
import * as lodash from 'lodash';

/**
 * Verify if the given token is valid, reject if not
 * @param token { string }
 */
export function verifyJWTToken(token) {
    return new Promise((resolve, reject) => {
        jwt.verify(token, process.env.JWT_SECRET, (err, decodedToken) => {
			if (err || !decodedToken)
				return reject(err);

          	resolve(decodedToken);
        });
    });
}

/**
 * create a token and return it
 * @param details { string }
 */
export function createJWToken(details) {
	if (typeof details !== 'object') 
		details = {};

	if (!details.maxAge || typeof details.maxAge !== 'number')
		details.maxAge = 3600

	details.sessionData = lodash.reduce(details.sessionData || {}, (memo, val, key) => {
		if (typeof val !== "function" && key !== "password")
            memo[key] = val;
            
		return memo;
	}, {});

	let token = jwt.sign({
		data: details.sessionData
		}, process.env.JWT_SECRET, {
		expiresIn: details.maxAge,
		algorithm: 'HS256'
	});

	return token;
}