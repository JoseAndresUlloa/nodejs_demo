import "reflect-metadata";
import express = require("express");
import { Router } from "express";
import * as createError from "http-errors";
import {createConnection} from "typeorm";
import * as bodyParser from "body-parser";
import { useExpressServer} from "routing-controllers";
import { authenticate, login } from "./security/auth";


// create db connection
createConnection({
    type: "mysql",
    host: process.env.DB_HOST,
    port: 3306,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    synchronize: false,
    logging: false,
    entities: [
        __dirname + '/entities/*{.ts,.js}'
    ],
    cli: {
       entitiesDir: __dirname + 'entities'
    }
}).then(connection => {    
    console.log("Connection to DB successful."); 

    // create express app
    const app = express();
    
    // midlewares
    app.use(bodyParser.json());
   
    // register controllers
    useExpressServer(app, {
        routePrefix: process.env.ROUTE,
        controllers: [__dirname + '/controllers/*{.ts,.js}'] 
    });

    // use token verification in all routes
    const router = Router();
    router.all(process.env.ROUTE + '/*', authenticate);
    // login route
    router.post('/login', login);
    // register validation and login route
    app.use('/', router); 

    // exception handler
    app.use((err, req, res, next) => {
        console.error(err.stack);
        res.status(err.status | 500).send({
            error: err,
            stack: process.env.NODE_ENV == 'production' ? '' : err.stack      
        })
    });

    // catch 404 and forward to error handler
    app.use((req, res, next) => {
        next(createError(404));
    });

    // start express server
    app.listen(process.env.PORT, () => {
        console.log(`Express server has started on port ${process.env.PORT}`);
        
        // start scheduled
        var tasks = require('./schedules/scheduled-tasks');
    });

}).catch(error => {
    console.log(error);
    createError(500);  
});







