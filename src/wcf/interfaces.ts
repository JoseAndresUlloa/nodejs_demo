import * as soap from 'soap';

export class Interfaces {
    private static schema;
    private static client;

    private static async create(){
        this.client = await soap.createClientAsync(process.env.INTERFACES_URL);  
        this.schema = this.client.describe();
    }

    static async getSocioByCardCode(pCardCode: string){
        await this.create();
        let result = await this.client.GetSocioByCardCodeAsync({pCardCode: pCardCode})
        return result[0].GetSocioByCardCodeResult;
    }
}
 