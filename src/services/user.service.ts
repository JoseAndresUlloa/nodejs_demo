import { getConnection } from "typeorm"
import { User } from "../entities/user"
import * as bcrypt from 'bcryptjs'

export class UserService {
    private repository = getConnection().getRepository(User)
    
    getAll() {
        return this.repository.find()
    }

    getOne(id: number) {
        return this.repository.findOne(id)
    }

    getByUsername(username: string) {
        return this.repository.findOne({username: username})
    }

    create(user: User) {
        user.password = bcrypt.hashSync(user.password, 10)
        return this.repository.save(user)
    }

    update(id: number, user: User) {
        user.password = bcrypt.hashSync(user.password, 10)
        return this.repository.update(id, user)
    }

    async delete(id: number) {
        let userToRemove = await this.repository.findOne(id)
        return this.repository.remove(userToRemove)
    }
}