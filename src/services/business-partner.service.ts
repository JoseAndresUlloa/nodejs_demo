import { getConnection } from "typeorm"
import { BusinessPartner } from "../entities/business-partner";
import { Interfaces } from "../wcf/interfaces";

export class BusinessPartnerService {
    private repository = getConnection().getRepository(BusinessPartner)

    getAll() {
        return this.repository.find()
    }

    getOne(id: number) {
        return this.repository.findOne(id)
    }

    create(businessPartner: BusinessPartner) {
        return this.repository.save(businessPartner)
    }

    update(id: number, businessPartner: BusinessPartner) {
        return this.repository.update(id, businessPartner)
    }

    async delete(id: number) {
        let bpToRemove = await this.repository.findOne(id)
        return this.repository.remove(bpToRemove)
    }

    getByCardCode(cardCode: string) {
        return Interfaces.getSocioByCardCode(cardCode)
    }
}